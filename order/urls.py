from rest_framework.routers import DefaultRouter

from . import views

app_name = 'order'

router = DefaultRouter()
router.register('order', views.OrderViewSet, base_name='order')
urlpatterns = router.urls
