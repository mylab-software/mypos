# Generated by Django 2.0.6 on 2018-07-03 17:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('item', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_datetime', models.DateTimeField(auto_now_add=True)),
                ('updated_datetime', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(choices=[('waiting', 'Waiting'), ('cooking', 'Cooking'), ('ready', 'Ready'), ('canceled', 'Canceled'), ('finished', 'Finished')], default='waiting', max_length=25)),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='item.Item')),
            ],
            options={
                'db_table': 'order',
            },
        ),
    ]
