from django.db import models

from base import models as base_models


class Order(base_models.ParentModel):
    WAITING = 'waiting'
    COOKING = 'cooking'
    READY = 'ready'
    CANCELED = 'canceled'
    FINISHED = 'finished'

    STATUS_CHOICES = (
        (WAITING, 'Waiting'),
        (COOKING, 'Cooking'),
        (READY, 'Ready'),
        (CANCELED, 'Canceled'),
        (FINISHED, 'Finished'),
    )

    item = models.ForeignKey('item.Item', on_delete=models.CASCADE)
    status = models.CharField(max_length=25, choices=STATUS_CHOICES, default=WAITING)

    class Meta:
        db_table = 'order'
