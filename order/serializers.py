from rest_framework import serializers

from item import models as item_models
from . import models as order_models


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = item_models.Item
        fields = ('id', 'name')


class OrderSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    item = ItemSerializer(read_only=True)
    item_id = serializers.IntegerField(write_only=True)
    created_datetime = serializers.DateTimeField(read_only=True)

    class Meta:
        model = order_models.Order
        fields = ('id', 'item', 'item_id', 'status', 'created_datetime')
