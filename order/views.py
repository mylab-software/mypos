from base import views as base_views
from . import models as order_models, serializers as order_serializers


class OrderViewSet(base_views.BaseViewSet):
    queryset = order_models.Order.objects.all()
    serializer_class = order_serializers.OrderSerializer

    def list(self, request, *args, **kwargs):
        qparams = request.query_params
        if 'status' in qparams:
            self.queryset.filter(status=qparams.get('status'))
        return super().list(request, *args, **kwargs)
