Test work backend of BurgerHouse
==================================


Prerequisites
-----------

- Required python 3.x installed

Installation and Usage
-----------

- `cd` into root folder
- install python dependencies: `pip install -r requirements.txt`
- migrate the database: `python manage.py migrate`
- load fixtures: `python manage.py loaddata initial_data.json`
- run server: `python manage.py runserver 0:8000`

