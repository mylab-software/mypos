OK = 0

SERVER_ERROR = 1
AUTHENTICATION_FAILED = 2
NOT_AUTHENTICATED = 3
PERMISSION_DENIED = 4
NOT_FOUND = 5
METHOD_NOT_ALLOWED = 6
NOT_ACCEPTABLE = 7
UNSUPPORTED_MEDIA_TYPE = 8
THROTTLED = 9
VALIDATION_FAILED = 10
PASSWORD_INCORRECT = 11
PASSWORD_MISMATCH = 11

REQUIRED_FIELD = 101
NOT_UNIQUE = 102

MESSAGES = {
    OK: 'OK',

    SERVER_ERROR: 'Server error',
    AUTHENTICATION_FAILED: 'Authentication failed',
    NOT_AUTHENTICATED: 'Not authenticated',
    PERMISSION_DENIED: 'Permission denied',
    METHOD_NOT_ALLOWED: 'Method not allowed',
    NOT_ACCEPTABLE: 'Not acceptable',
    UNSUPPORTED_MEDIA_TYPE: 'Unsupported media type',
    THROTTLED: 'Throttled',
    VALIDATION_FAILED: 'Validation failed',
    NOT_FOUND: 'Not found',
    PASSWORD_INCORRECT: 'Password incorrect',
    PASSWORD_MISMATCH: 'Password mismatch',

    REQUIRED_FIELD: 'Cannot be empty',
    NOT_UNIQUE: 'Not unique'
}
