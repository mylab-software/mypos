from rest_framework.views import exception_handler
from rest_framework.exceptions import AuthenticationFailed, NotAuthenticated, PermissionDenied, NotFound, \
    MethodNotAllowed, NotAcceptable, UnsupportedMediaType, Throttled
from rest_framework.response import Response
from rest_framework import status, pagination

from . import brc


def make_business_response(code=None, message=None, data={}, errors={}):
    return {
        'code': code,
        'message': message if message else brc.MESSAGES.get(code),
        'bdata': data,
        'errors': errors
    }


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)

    if response is not None:
        if isinstance(exc, AuthenticationFailed):
            response.data = make_business_response(brc.AUTHENTICATION_FAILED, message=str(exc))
        elif isinstance(exc, NotAuthenticated):
            response.data = make_business_response(brc.NOT_AUTHENTICATED, message=str(exc))
        elif isinstance(exc, PermissionDenied):
            response.data = make_business_response(brc.PERMISSION_DENIED, message=str(exc))
        elif isinstance(exc, NotFound):
            response.data = make_business_response(brc.NOT_FOUND, message=str(exc))
        elif isinstance(exc, MethodNotAllowed):
            response.data = make_business_response(brc.METHOD_NOT_ALLOWED, message=str(exc))
        elif isinstance(exc, NotAcceptable):
            response.data = make_business_response(brc.NOT_ACCEPTABLE, message=str(exc))
        elif isinstance(exc, UnsupportedMediaType):
            response.data = make_business_response(brc.UNSUPPORTED_MEDIA_TYPE, message=str(exc))
        elif isinstance(exc, Throttled):
            response.data = make_business_response(brc.THROTTLED, message=str(exc))
    else:
        response = Response(data=make_business_response(brc.SERVER_ERROR, message=str(exc)),
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return response


class CustomLimitOffsetPagination(pagination.LimitOffsetPagination):

    def get_paginated_response(self, data):
        result_data = {
            "count": self.count,
            "next": self.get_next_link(),
            "previous": self.get_previous_link(),
            "results": data
        }
        return Response(data=make_business_response(code=brc.OK, data=result_data),
                        status=status.HTTP_200_OK)
