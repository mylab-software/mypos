from rest_framework import serializers

from . import brc, utils


def not_empty(value):
    if not value:
        response = utils.make_business_response(code=brc.REQUIRED_FIELD)
        raise serializers.ValidationError(response)
