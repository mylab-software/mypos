from django.http import Http404

from rest_framework import viewsets, status
from rest_framework.response import Response

from . import brc, utils


class BaseViewSet(viewsets.ModelViewSet):

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(data=utils.make_business_response(code=brc.OK, data=serializer.data),
                        status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        kwargs['data'] = request.data
        serializer = self.get_serializer(*args, **kwargs)
        if serializer.is_valid():
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(data=utils.make_business_response(code=brc.OK, data=serializer.data),
                            status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response(data=utils.make_business_response(code=brc.VALIDATION_FAILED,
                                                              errors=serializer.errors),
                            status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(data=utils.make_business_response(code=brc.OK, data=serializer.data),
                        status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        if serializer.is_valid():
            self.perform_update(serializer)

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

            return Response(data=utils.make_business_response(code=brc.OK, data=serializer.data),
                            status=status.HTTP_200_OK)
        else:
            return Response(data=utils.make_business_response(code=brc.VALIDATION_FAILED,
                                                              errors=serializer.errors),
                            status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(data=utils.make_business_response(code=brc.OK),
                        status=status.HTTP_204_NO_CONTENT)

    def handle_exception(self, exc):
        if isinstance(exc, Http404):
            return Response(data=utils.make_business_response(code=brc.NOT_FOUND), status=status.HTTP_404_NOT_FOUND)
        return super().handle_exception(exc)
