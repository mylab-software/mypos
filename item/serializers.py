from rest_framework import serializers

from . import models as item_models


class ItemSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    price = serializers.DecimalField(max_digits=19, decimal_places=2, coerce_to_string=False)

    class Meta:
        model = item_models.Item
        fields = ('id', 'name', 'price', 'image')
