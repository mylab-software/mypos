from rest_framework.routers import DefaultRouter

from . import views

app_name = 'item'

router = DefaultRouter()
router.register('item', views.ItemViewSet, base_name='item')
urlpatterns = router.urls
