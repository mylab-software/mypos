from django.db import models

from base import models as base_models


class Item(base_models.ParentModel):
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=19, decimal_places=2, default=0)
    image = models.CharField(max_length=500)

    class Meta:
        db_table = 'item'
