from base import views as base_views
from . import models as item_models, serializers as item_serializers


class ItemViewSet(base_views.BaseViewSet):
    queryset = item_models.Item.objects.all()
    serializer_class = item_serializers.ItemSerializer
